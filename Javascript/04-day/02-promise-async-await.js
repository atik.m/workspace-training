var fetchCustomers = async function(){
    var hello = function (){ console.log('hello..')}

      let promise = new Promise((resolve, reject) => {
            setTimeout(() => {
                hello();
                resolve("done!");
                reject("rejected!"); // I have done it
            }, 2000)
          });
      // will ne met in future OR it is already done
      //

     await promise; // wait until the promise resolves (*)
    //await setTimeout(hello , 1000);
    // await works ONLY in async + statement that return promise
    console.log('before FETCH');
    await fetch("http://localhost:3000/customer", {
        method: 'get',
            headers: {
            'Content-Type': 'application/json;charset=utf-8'
              }
          })
      .then(response => response.json())
      .then(response => {
          console.log('hello fetch');
          customers = response;
          console.log('customer lengthY:'+customers.length);
          showCustomers();
      });

      console.log('customer lengthX:'+customers.length);
    console.log('hello Dian');
    console.log('hello Vivek');
}



//03-day promise

let promise = new Promise((resolve, reject) => {
    setTimeout(() => {
      //  resolve("done!");
        reject("rejected!"); // I have done it
    }, 6000)
  });