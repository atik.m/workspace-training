b = 90; 
console.log("b is "+ typeof b)

c = "China"; 
console.log("c is "+ typeof c)

//d = "China"; 
console.log("d is "+ typeof d)

e = true; 
console.log("e is "+ typeof e)

f = function (a,b) {
	return a+b;
}; 
console.log("f is "+ typeof f)

g = new Object(); // {}

console.log("g is "+ typeof g)