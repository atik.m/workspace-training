
var input = [11, 11, 3, 4, 5, 12, 11, 45, 11, 11];

var checkAndgetMostFreNumber = (array)=>{
// SQ with 2 for loops 
// Ming one for loops 
	var mostFrquentNumber = NaN;
	var numberCount = NaN;
	var trackObject = {};
	// key:value

	for (var i = 0; i <  array.length; i++) { //10 times 10 seconds
		if(typeof(trackObject[array[i]]) == 'undefined' ){
			trackObject[array[i]] = 1;
		} else{
			trackObject[array[i]] += 1;
		}

		if(isNaN(mostFrquentNumber)){
			mostFrquentNumber = array[i];
			numberCount = 1;
		}else {
			if(trackObject[array[i]] > numberCount){
				numberCount = trackObject[array[i]];
				mostFrquentNumber = array[i];
			}
		}
	}
	console.log(JSON.stringify(trackObject));

	return {mostFrequent:mostFrquentNumber, count:numberCount}
}

// n x n 
var result = checkAndgetMostFreNumber(input);
console.log("Frequent number: "+result.mostFrequent +" with count "+result.count);

input = [12, 11, 3, 4, 5, 12, 11, 45, 87, 12,5,5,5];

result = checkAndgetMostFreNumber(input);
console.log("Frequent number: "+result.mostFrequent +" with count "+result.count);

input = [12, 11, 11, 3, 4, 5, 11,12, 11, 45,11, 87, 12,5,5,5];

result = checkAndgetMostFreNumber(input);
console.log("Frequent number: "+result.mostFrequent +" with count "+result.count);

