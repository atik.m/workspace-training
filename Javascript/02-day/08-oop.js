// OOPS in JavaScript Encapsulation 
var Person = function(){
    // name is private
    var name="-";
    this.getName = () => name;
    this.setName = (nameVal) => {name = nameVal};

    //email is public
    this.email = "email";
    this.getEmail = () => this.email;
    this.setEmail = (email) => {this.email = email};
}

var p = new Person();
    console.log("name 1: "+ p.getName());
    p.setName("Rama");
    console.log("name direct: "+ p.name);
    console.log("name 2: "+ p.getName());


    console.log("email 1: "+ p.getEmail());
    p.setEmail("rama@india.com");
    console.log("email direct: "+ p.email);
    console.log("email 2: "+ p.getEmail());

var p2 = new Person();
    console.log("name 1: "+ p2.getName());
    p2.setName("Vivek");
    console.log("name direct: "+ p.name);
    console.log("name 2: "+ p.getName());


    console.log("email 1: "+ p.getEmail());
    p.setEmail("rama@india.com");
    console.log("email direct: "+ p.email);
    console.log("email 2: "+ p.getEmail());


    //inheritance

class Bicycle {// the Bicycle class has one constructor
    constructor(gear, speed) {
       console.log(" Bicycle constructor");
       this.gear = gear;
       this.speed = speed;
    }
}â€¨
class MountainBike extends Bicycle { // the MountainBike subclass has one constructor
    constructor(gear, speed, startHeight) {
       // invoking base-class(Bicycle) constructor
       super(gear, speed);
       console.log(" MountainBike constructor");
       this.seatHeight = startHeight;
    }
}

var bCycle = new MountainBike(3,6);
var mCycle = new MountainBike(3,6,5);
