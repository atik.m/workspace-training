// vivek  ameer  army 
// this is a point to object whihc is the nearnest parent object
var a ='Amar';
var obj = {
	//code specify relation
	// var , let , this, : global 
	sample:'vivek',

    doSomething: function () { // doSomething is child of obj
        this.a = "bob"; //obj.a    
        var name = 'vivek'; // 
        //code is part of obj , doSomething
        function doAnotherThing () { //defined var no / : this
            console.log("Name1: " + this.a);  //window.a 
        };

        console.log("Name2: " + this.a); // bob
        doAnotherThing();
    }
};
//What does this print?
obj.doSomething();





var obj = {
	doSomething: function () {
		this.a = "bob"; //obj.a 
		function doAnotherThing () { //win
			console.log("Name1: " + this.a);
		};
		console.log("Name2: " + this.a); // bob
		doAnotherThing();
	}
};
//What does this print?
obj.doSomething();