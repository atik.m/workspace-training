var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

var customers = [ 
	{id:1,name:'Vivek', email:'vivek@abc.com', phone:'8989389333',address:"Singapore"},
	{id:2,name:'Dev', email:'dev@abc.com', phone:'866u389333',address:"India"},
	{id:3,name:'Ameer', email:'ameer@abc.com', phone:'877779333',address:"Asia"},
	{id:4,name:'Shen', email:'shen@abc.com', phone:'7873653535',address:"Singapore"},
];


// get all
app.get('/api/customer',(req,res)=>{
	res.send(customers);
})

// get by Id
app.get('/api/customer/:id',(req,res)=>{
	let customer = {};
	let temp = customers.filter((item)=>(item.id == req.params.id));
	if(temp.length > 0){
		customer = temp[0];
	}
	res.send(customer);
})
/*
// get all
app.get('/api/customer/:field/:searchText',(req,res)=>{
	var field = req.params.field.toLowerCase();
	var searchText = req.params.searchText.toLowerCase();
	var validFields = ['name', 'email','address', 'phone'];

	if(validFields.indexOf(field) > -1 ){
		let searchCustomer = customers.filter((item)=>{
		//console.log(item[field].toLowerCase() +item[field].toLowerCase().startsWith(searchText));
		return (item[field].toLowerCase().startsWith(searchText));
		});
		res.send(searchCustomer);
	}	else{
		res.send([]);
	}
}) */

app.get('/api/customer/:field/:searchText',(req,res)=>{
	var field = req.params.field.toLowerCase();
	var searchText = req.params.searchText.toLowerCase();
	var validFields = ['name', 'email','address', 'phone'];
	let searchCustomer = [];
	try{
		searchCustomer = customers.filter((item)=>{
		//console.log(item[field].toLowerCase() +item[field].toLowerCase().startsWith(searchText));
		return (item[field].toLowerCase().startsWith(searchText));
		});
		console.log("ONLY on success");
	}catch (e){
		console.log("error is "+e);
	}
	finally{ //must execute 
		res.send(searchCustomer);
	}
})

// add new
app.post('/api/customer',(req,res)=>{
	req.body.id = Date.now();
	customers.push(req.body);
	res.send({result:'success', msg:"customer added successfully"});
})

// update
app.put('/api/customer',(req,res)=>{
	customer = req.body;
	for (var i = 0; i < customers.length; i++) {
		if(customers[i].id == customer.id){
			customers[i] = customer;
		}
	}
	res.send({result:'success', msg:"customer updated successfully"});
})

// delete
app.delete('/api/customer',(req,res)=>{
	let temp = customers.filter((item)=>(item.id != req.body.id));
	customers = temp;
	res.send({result:'success', msg:"customer deleted successfully"});
})

app.get('/api/login',(req,res)=>{
	res.send("hello login");
})

app.post('/api/login',(req,res)=>{
	//console.log("Header "+ JSON.stringify(req.header));
	console.log("Body "+ JSON.stringify(req.body));
	if(req.body.email != undefined 
		&& req.body.email != ""
		&& req.body.email == req.body.password ){
		res.send({result:'success', msg:"login successful"});
	}else{
		res.send({result:'fail', msg:"login failed"});
	}
})


app.get('/api/hello',(req,res)=>{
	res.send("hello login 1111");
})

app.get('/api/training',(req,res)=>{
	res.send(" training ....hello login 1111");
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
